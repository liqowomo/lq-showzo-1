<h1 align="center"><code>🌸: lq:Showzo-1</code></h1>
<h2 align="center"><i> Testing out gfx stuff </i></h2>

----
1. [Wat ?](#wat-)
   1. [Motion-Canvas](#motion-canvas)
2. [Manim Installs](#manim-installs)

----

# Wat ?

1. This is going be testing of the following 

|                      🏩                      |              ⌛              |
| :-----------------------------------------: | :-------------------------: |
|   [`Manim`](https://www.manim.community/)   |    Animations in python     |
| [`Motion-Canvas`](https://motioncanvas.io/) | Typescript based animations |

## Motion-Canvas 
1. Little too complicated 
2. Stick with manim 

# Manim Installs 

step 1 : Prerequisites 

```sh 
sudo apt update
sudo apt install build-essential python3-dev libcairo2-dev libpango1.0-dev ffmpeg
```

step 2 : Python Install

```sh 
pip install manim
```

![](./mn1/media/videos/t1/1080p60/t11.mp4)